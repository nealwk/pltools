# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import pdb

from pltools.campaign_manager import CampaignManager

import pytest


def test_get_campaigns(campaigns):
    cm = CampaignManager('', '')
    try:
        for campaign in campaigns:
            cm.driver.get(campaign['url'])

            result = cm.get_campaigns()
            assert result

            for item in campaign['campaigns']:
                assert item in result
    finally:
        cm.driver.close()

def test_get_campaign_budget(campaigns):
    cm = CampaignManager('', '')
    try:
        for campaign in campaigns:
            cm.driver.get(campaign['url'])

            for item in campaign['campaigns']:
                if item['state'] != 'Enabled':
                    continue

                budget = cm.get_campaign_budget(item['name'])
                assert budget == item['budget']
    finally:
        cm.driver.close()
