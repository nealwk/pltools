# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from pltools.order_fulfiller import OrderFulfiller

import pytest


def test_get_product(products):
    fulfiller = OrderFulfiller('', '')
    try:
        for product in products:
            fulfiller.driver.get(product['url'])

            result = fulfiller.get_product(product['sku'])
            assert result
            assert result['element'] is not None
            assert result['status'] == product['status']
    finally:
        fulfiller.driver.close()
