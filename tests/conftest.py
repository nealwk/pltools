# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os
import glob

from pltools import init_web_driver

import pytest


@pytest.fixture(scope='session')
def inventory_pages_dir():
    return os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'pages', 'inventory')

@pytest.fixture(scope="session")
def advertising_pages_dir():
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pages', 'advertising')

@pytest.fixture(scope='session')
def products(inventory_pages_dir):
    return [
        {
            'sku': '7Y-XLF4-54MC',
            'url': 'file://' + os.path.join(inventory_pages_dir, 'inv_sku.mhtml'),
            'status': 'inactive'
        },
        {
            'sku': 'L5-36QC-98NY',
            'url': 'file://' + os.path.join(inventory_pages_dir, 'inv_sku_active.mhtml'),
            'status': 'active'
        }
    ]

@pytest.fixture(scope='session')
def campaigns(advertising_pages_dir):
    return [
        {
            'url': 'file://' + os.path.join(advertising_pages_dir, 'campaign.mhtml'),
            'campaigns': [
                {
                    'name': 'foot-peel-mask_sellermotor_stephanie_CedarTree_Manual',
                    'state': 'Enabled',
                    'status': 'Delivering',
                    'budget': 50.0,
                    'impressions': 6949,
                    'clicks': 15,
                    'ctr': 0.22,
                    'spend': 17.08,
                    'cpc': 1.14
                },
                {
                    'name': 'eye-cream_sellermotor_stephanie_CedarTree_Manual',
                    'state': 'Enabled',
                    'status': 'Delivering',
                    'budget': 50.0,
                    'impressions': 7462,
                    'clicks': 31,
                    'ctr': 0.42,
                    'spend': 33.41,
                    'cpc': 1.08
                },
                {
                    'name': 'skin-tag-remover-patch_sellermotor_stephanie_CedarTree_Manual',
                    'state': 'Enabled',
                    'status': 'Out of budget',
                    'budget': 250.0,
                    'impressions': 13770,
                    'clicks': 318,
                    'ctr': 2.31,
                    'spend': 454.71,
                    'cpc': 1.43
                },
            ]
        },
        {
            'url': 'file://' + os.path.join(advertising_pages_dir, 'campaign_2.mhtml'),
            'campaigns': [
                {
                    'name': 'B08971P3J4_blackhead-remover-mask_sellermotor_stephanie_Blessing--Store_Manual',
                    'state': 'Enabled',
                    'status': 'Delivering',
                    'budget': 20.0,
                    'impressions': 4,
                    'clicks': 0,
                    'ctr': 0,
                    'spend': 0,
                    'cpc': 0
                },
                {
                    'name': 'hair-removal-cream-men_sellermotor_stephanie_Blessing--Store_Manual',
                    'state': 'Enabled',
                    'status': 'Delivering',
                    'budget': 40.0,
                    'impressions': 1924,
                    'clicks': 7,
                    'ctr': 0.36,
                    'spend': 4.62,
                    'cpc': 0.66
                },
                {
                    'name': 'B07ZWG4V7N DIBID EXACT',
                    'state': 'Enabled',
                    'status': 'Delivering',
                    'budget': 45.0,
                    'impressions': 629,
                    'clicks': 3,
                    'ctr': 0.48,
                    'spend': 6.41,
                    'cpc': 2.14
                },
            ]
        }
    ]
