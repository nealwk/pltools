# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    TimeoutException, NoSuchElementException, WebDriverException,
    StaleElementReferenceException)
from selenium.webdriver.support.select import Select
import us
import pycountry
import phonenumbers

from pltools.seller_central_base import SellerCentralBase
from pltools import logger, MARKETPLACE_MAPPING


class CampaignManager(SellerCentralBase):
    def get_campaigns(self):
        result = True

        campaigns = dict()
        campaigns_table_xpath = '//div[@id="CAMPAIGNS" and @data-e2e-id="table"]'
        try:
            WebDriverWait(self.driver, 3).until(
                EC.visibility_of_element_located((By.XPATH, campaigns_table_xpath)))
        except Exception as e:
            logger.exception(e)
            result = False
        else:
            state_elems = self.driver.find_elements_by_xpath(
                '//div[@data-udt-column-id="state-cell"]')
            for state_elem in state_elems:
                idx = state_elem.get_attribute('data-e2e-index').split('_')
                idx.pop()
                idx = '_'.join(idx)
                campaigns.setdefault(idx, dict())

                button_elem = state_elem.find_element_by_xpath('./div/div/button')
                state = button_elem.get_attribute('title')

                campaigns[idx]['state'] = state

        name_elems = self.driver.find_elements_by_xpath('//div[@data-udt-column-id="name-cell"]')
        for name_elem in name_elems:
            idx = name_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = name_elem.find_element_by_xpath('./div/div/a')
            name = elem.text.strip()
            campaigns[idx]['name'] = name

        status_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="status-cell"]')
        for status_elem in status_elems:
            idx = status_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = status_elem.find_element_by_xpath('./div/div[@data-e2e-id="statusText"]')
            status = elem.get_attribute('title')
            campaigns[idx]['status'] = status

        budget_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="budget-cell"]')
        for budget_elem in budget_elems:
            idx = budget_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = budget_elem.find_element_by_xpath('.//input[@data-e2e-id="currencyInput"]')
            budget_str = elem.get_attribute('value').replace(',', '')
            try:
                budget = round(float(budget_str), 2)
                campaigns[idx]['budget'] = budget
            except Exception as e:
                logger.exception(e)
                continue

        impressions_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="impressions-cell"]')
        for impressions_elem in impressions_elems:
            idx = impressions_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = impressions_elem.find_element_by_xpath(
                './/div[@data-e2e-id="numberRenderer"]')
            impressions_str = elem.text.strip().replace(',', '')
            if not impressions_str or impressions_str == '-':
                impressions = 0
            else:
                try:
                    impressions = int(impressions_str)
                except Exception as e:
                    impressions = 0
                    logger.exception(e)
            campaigns[idx]['impressions'] = impressions

        click_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="clicks-cell"]')
        for click_elem in click_elems:
            idx = click_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = click_elem.find_element_by_xpath('.//div[@data-e2e-id="numberRenderer"]')
            clicks_str = elem.text.strip().replace(',', '')
            if not clicks_str or clicks_str == '-':
                clicks = 0
            else:
                try:
                    clicks = int(clicks_str)
                except Exception as e:
                    clicks = 0
                    logger.exception(e)
            campaigns[idx]['clicks'] = clicks

        ctr_elems = self.driver.find_elements_by_xpath('//div[@data-udt-column-id="ctr-cell"]')
        for ctr_elem in ctr_elems:
            idx = ctr_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = ctr_elem.find_element_by_xpath('.//div[@data-e2e-id="percentRenderer"]')
            ctr_str = elem.text.strip().replace('%', '')
            if not ctr_str or ctr_str == '-':
                ctr = 0
            else:
                try:
                    ctr = round(float(ctr_str), 2)
                except Exception as e:
                    logger.exception(e)
                    ctr = 0
            campaigns[idx]['ctr'] = ctr

        spend_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="spend-cell"]')
        for spend_elem in spend_elems:
            idx = spend_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = spend_elem.find_element_by_xpath('.//div[@data-e2e-id="currencyRenderer"]')
            spend_str = elem.text.strip().replace('$', '').replace(',', '')
            if not spend_str or spend_str == '-':
                spend = 0
            else:
                try:
                    spend = round(float(spend_str), 2)
                except Exception as e:
                    logger.exception(e)
                    spend = 0
            campaigns[idx]['spend'] = spend

        cpc_elems = self.driver.find_elements_by_xpath(
            '//div[@data-udt-column-id="cpc-cell"]')
        for cpc_elem in cpc_elems:
            idx = cpc_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)
            if idx not in campaigns:
                continue

            elem = cpc_elem.find_element_by_xpath('./div/div[@data-e2e-id="currencyRenderer"]')
            cpc_str = elem.get_attribute('textContent').strip().replace('$', '').replace(',', '')
            if not cpc_str or cpc_str == '-':
                cpc = 0
            else:
                try:
                    cpc = round(float(cpc_str), 2)
                except Exception as e:
                    logger.exception(e)
                    cpc = cpc_str
            campaigns[idx]['cpc'] = cpc

        return list(campaigns.values())

    def set_campaign_budget(self, name, budget):
        result = True

        elem = self.get_campaign_budget_elem(name)
        if not elem:
            return False

        elem.click()

        budget_adjust_xpath = '//div[@id="portal"]//input[@data-e2e-id="editableCurrencyInput"]'
        try:
            WebDriverWait(self.driver, 3).until(
                EC.visibility_of_element_located((By.XPATH, budget_adjust_xpath)))
        except Exception as e:
            logger.exception(e)
            result = False
        else:
            input_elem = self.driver.find_element_by_xpath(budget_adjust_xpath)
            input_elem.clear()
            input_elem.send_keys(budget)

            save_elem = self.driver.find_element_by_xpath(
                '//div[@id="portal"]//button[@data-e2e-id="saveButton"]')
            save_elem.click()

            cur_budget = self.get_campaign_budget(name)
            result = cur_budget == budget

        return result

    def get_campaign_budget(self, name):
        elem = self.get_campaign_budget_elem(name)
        if elem:
            budget_str = elem.get_attribute('value')
            budget = round(float(budget_str), 2)
        else:
            budget = 0

        return budget

    def get_campaign_budget_elem(self, name):
        try:
            xpath = '//div[@data-udt-column-id="name-cell" and .//a[contains(text(), "{}")]]'.format(
                name)
            campaign_elem = self.driver.find_element_by_xpath(xpath)
            idx = campaign_elem.get_attribute('data-e2e-index').split('_')
            idx.pop()
            idx = '_'.join(idx)

            xpath = '//div[@data-udt-column-id="budget-cell" and contains(@data-e2e-index, "{}")]'.format(idx)
            budget_elem = self.driver.find_element_by_xpath(xpath)
            input_elem = budget_elem.find_element_by_xpath('.//input[@data-e2e-id="currencyInput"]')
        except Exception as e:
            logger.exception(e)

            input_elem = None

        return input_elem
