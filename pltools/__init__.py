# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os
import sys
import logging
from time import sleep

import chromedriver_autoinstaller
from selenium import webdriver


# Setup logging
logger = logging.getLogger('PLTools')
formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')
logger.setLevel(logging.DEBUG)
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


# Setup chrome driver
chromedriver_autoinstaller.install()

def init_web_driver(email):
    options = webdriver.ChromeOptions()
    ddir = os.path.join('C:\\', 'Tmp', 'pltools', email.lower())
    options.add_argument(f'user-data-dir={ddir}')
    options.add_argument('--disable-extensions')
    options.add_argument('--start-maximized')
    options.add_argument('--disable-notifications')
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    options.add_experimental_option('prefs', {'profile.default_content_setting_values.notifications': 2})

    driver = webdriver.Chrome(options=options)
    driver.set_page_load_timeout(30)
    driver.implicitly_wait(7)

    return driver

MARKETPLACE_MAPPING = {
    'us': {
        'sellercentral': 'sellercentral.amazon.com',
        'domain': 'www.amazon.com'
    },
    'ca': {
        'sellercentral': 'sellercentral.amazon.ca',
        'domain': 'www.amazon.ca'
    },
    'mx': {
        'sellercentral': 'sellercentral.amazon.com.mx',
        'domain': 'www.amazon.com.mx'
    },
    'uk': {
        'sellercentral': 'sellercentral.amazon.co.uk',
        'domain': 'www.amazon.co.uk'
    },
    'de': {
        'sellercentral': 'sellercentral.amazon.de',
        'domain': 'www.amazon.de'
    },
    'fr': {
        'sellercentral': 'sellercentral.amazon.fr',
        'domain': 'www.amazon.fr'
    },
    'it': {
        'sellercentral': 'sellercentral.amazon.it',
        'domain': 'www.amazon.it'
    },
    'es': {
        'sellercentral': 'sellercentral.amazon.es',
        'domain': 'www.amazon.es'
    },
    'jp': {
        'sellercentral': 'sellercentral.amazon.co.jp',
        'domain': 'www.amazon.co.jp'
    },
    'au': {
        'sellercentral': 'sellercentral.amazon.com.au',
        'domain': 'www.amazon.com.au'
    },
    'in': {
        'sellercentral': 'sellercentral.amazon.in',
        'domain': 'www.amazon.in'
    },
    'cn': {
        'sellercentral': 'mai.amazon.cn',
        'domain': 'www.amazon.cn'
    }
}
