# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import datetime

from pltools.campaign_manager import CampaignManager

from pltools import logger, MARKETPLACE_MAPPING


class BudgetAdjustTask():
    def __init__(self, cfg):
        self.cfg = cfg

    def run(self):
        accounts = self.cfg['seller'].get('accounts', [])
        if not accounts:
            return

        email = self.cfg['seller']['email']
        password = self.cfg['seller']['password']
        cm = CampaignManager(email, password)
        try:
            for account in accounts:
                name = account['name'].lower()
                marketplace = name[-2:]

                now = datetime.datetime.now()
                for adjustment in account['adjustments']:
                    hour = adjustment['hour']
                    budget = adjustment['budget']

                    if now.hour != hour:
                        continue

                    sellercentral = MARKETPLACE_MAPPING[marketplace]['sellercentral']
                    sellercentral_home_url = 'https://{}'.format(sellercentral)
                    result = cm.procceed_to_url(marketplace, sellercentral_home_url)
                    if not result:
                        continue

                    url = 'https://{}/cm/ref=xx_cmpmgr_dnav_xx'.format(sellercentral)
                    cm.procceed_to_url(None, url)

                    campaigns = cm.get_campaigns()

                    for campaign in campaigns:
                        if campaign['state'] != 'Enabled' or campaign['budget'] == budget:
                            continue

                        old_budget = campaign['budget']
                        cm.set_campaign_budget(campaign['name'], budget)
                        new_budget = cm.get_campaign_budget(campaign['name'])

                        logger.info(
                            '[CampaignBudgetAdjustment] name: %s, old_budget: %s, new_budget: %s',
                            campaign['name'], old_budget, new_budget)
        finally:
            cm.driver.quit()
