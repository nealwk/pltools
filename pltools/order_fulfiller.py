# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    TimeoutException, NoSuchElementException, WebDriverException,
    StaleElementReferenceException)
from selenium.webdriver.support.select import Select
import us
import pycountry
import phonenumbers

from pltools.seller_central_base import SellerCentralBase
from pltools import logger, MARKETPLACE_MAPPING


class OrderFulfiller(SellerCentralBase):
    def place_order(self, order):
        required_fields = [
            'ship-address-1', 'ship-city', 'ship-state', 'ship-postal-code', 'ship-country',
            'recipient-name', 'order_id', 'quantity-purchased'
        ]
        for required_field in required_fields:
            if required_field not in order:
                return False

        state = us.states.lookup(order['ship-state'])
        if state is None:
            logger.warning('Could not recognize state - %s', order)
            return False

        try:
            if order['ship-country'].lower() == 'uk':
                ship_country = 'GB'
            else:
                ship_country = order['ship-country']
            country = pycountry.countries.lookup(ship_country)
        except LookupError:
            country = None
        if country is None:
            logger.warning('Could not recognize country - %s', order)
            return False

        # orderId
        self.fill_field(self.driver.find_element_by_name('orderId'), order['order_id'])

        # sellables.0
        self.fill_field(
            self.driver.find_element_by_name('sellables.0'), order['quantity-purchased'])

        # fullName, phoneNumber
        self.fill_field(self.driver.find_element_by_name('fullName'), order['recipient-name'])

        # line1, line2, city
        line1_input = self.driver.find_element_by_xpath(
            '//input[not(@disabled) and @name="line1"]')
        self.fill_field(line1_input, order['ship-address-1'])
        if 'ship-address-2' in order:
            line2_input = self.driver.find_element_by_xpath(
                '//input[not(@disabled) and @name="line2"]')
            self.fill_field(line2_input, order['ship-address-2'])
        city_input = self.driver.find_element_by_xpath('//input[not(@disabled) and @name="city"]')
        self.fill_field(city_input, order['ship-city'])

        # regionCode
        region_elem = Select(self.driver.find_element_by_id('regionCode'))
        region_elem.select_by_value(state.abbr)

        # postalCode
        self.fill_field(self.driver.find_element_by_name('postalCode'), order['ship-postal-code'])

        # countryCode
        country_elem = Select(self.driver.find_element_by_id('countryCode'))
        country_elem.select_by_value(country.alpha_2)

        # phoneNumber
        if 'ship-phone-number' in order:
            phone_number = order['ship-phone-number']
            phone = phonenumbers.parse(phone_number, country.alpha_2)
            if phonenumbers.is_valid_number(phone):
                self.fill_field(
                    self.driver.find_element_by_name('phoneNumber'),
                    f'{phone.national_number}({phone.extension})')

        self.driver.find_element_by_name('Continue').click()
        try:
            place_order_elem = WebDriverWait(self.driver, 3).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@name="Place Order"]')))
            place_order_elem.click()
        except Exception as e:
            logger.exception(e)

        time.sleep(3)

        # Check success indicator or search by order id directly
        return self.is_fulfilled(order['marketplace'], order['order_id'])

    def trigger_create_fulfillment(self, marketplace, sku):
        marketplace = marketplace.lower()
        sc_domain = MARKETPLACE_MAPPING[marketplace]['sellercentral']
        url = f'https://{sc_domain}/inventory/ref=xx_invmgr_dnav_xx?tbla_myitable=sort:%7B%22sortOrder%22%3A%22DESCENDING%22%2C%22sortedColumnId%22%3A%22date%22%7D;search:{sku};pagination:1;'
        self.procceed_to_url(marketplace, url)

        product = self.get_product(sku)

        product_elem = product['element']
        # status = product['status']
        # if status != 'active':
        #     logger.warning('[SkuInactive] sku: %s, status: %s', sku, status)
        #     return False

        try:
            action_elem = product_elem.find_element_by_xpath(
                '//span[contains(@class, "a-button-splitdropdown")]')
                # '//span[contains(@class, "a-button-splitdropdown")]//button[@data-action="a-splitdropdown-button"]')
            if action_elem is None:
                logger.warning('Could not find action button - %s', sku)
                return False

            action_elem.click()
            time.sleep(1)

            result = False
            dd_menus = self.driver.find_elements_by_css_selector('li.a-dropdown-item')
            for dm in dd_menus:
                dm_title = dm.get_attribute('innerText').lower()
                if 'create' in dm_title and 'fulfillment' in dm_title and 'order' in dm_title:
                    dm.click()
                    result = True
                    break

        except (NoSuchElementException, TimeoutException):
            logger.warning('Could not find sku - %s', sku)
            result = False

        return result

    def get_product(self, sku):
        sku_xpath = f'//div[@data-column="sku"]/div/a[contains(text(), "{sku}")]'
        try:
            sku_elem = WebDriverWait(self.driver, 3).until(
                EC.visibility_of_element_located((By.XPATH, sku_xpath)))

            sku_wrapper_elem = sku_elem.find_element_by_xpath('./parent::div/parent::div')
            if sku_wrapper_elem is None:
                logger.warning('Could not find sku wrapper - %s', sku)
                return False

            item_id = sku_wrapper_elem.get_attribute('data-row')
            item_elem = self.driver.find_element_by_xpath(f'//tr[@id="{item_id}"]')

            status_elem = item_elem.find_element_by_xpath('//div[@data-column="status"]/div/span')
            if status_elem is None:
                logger.warning('Could not find product status')
                return False

            result = {
                'element': item_elem,
                'status': status_elem.text.strip().lower()
            }
        except (NoSuchElementException, TimeoutException):
            logger.warning('Could not find product - %s', sku)
            result = False

        return result

    def is_fulfilled(self, marketplace, order_id):
        marketplace = marketplace.lower()
        sc_domain = MARKETPLACE_MAPPING[marketplace]['sellercentral']
        url = 'https://{}/orders-v3/search?page=1&q={}&qt=orderid&date-range=last-30'.format(
            sc_domain, order_id)
        self.procceed_to_url(marketplace, url)

        fulfilled = True
        order_xpath = '//table[@id="orders-table"]/tbody/tr/td//a[contains(text(), "{}")]'.format(
            order_id)
        while True:
            try:
                WebDriverWait(self.driver, 3).until(
                    EC.visibility_of_element_located((By.XPATH, order_xpath)))
                break
            except StaleElementReferenceException:
                pass
            except (NoSuchElementException, TimeoutException):
                fulfilled = False
                break

        return fulfilled
