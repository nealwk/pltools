# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from googleapiclient import errors

from pltools import logger


class GoogleSheetOrder():
    def __init__(self, google_sheet_service, num_retries=6):
        self.service = google_sheet_service
        self.num_retries = num_retries
        self.sheets_to_ignore = ['template', 'test']

    def load_orders(self, spreadsheet_id):
        d = dict()
        required_header_columns = [
            'order-acc', 'customer-id', 'fulfillment-acc', 'fulfillment-sku', 'order-id']

        req = self.service.spreadsheets().get(spreadsheetId=spreadsheet_id, includeGridData=False)
        try:
            resp = req.execute(num_retries=self.num_retries)
            logger.debug('Spreadsheet %s - %s', spreadsheet_id, resp)

            for sheet in resp.get('sheets', []):
                sheet_id = sheet.get('properties', d).get('sheetId', None)
                sheet_title = sheet.get('properties', d).get('title', None)
                if sheet_id is None or sheet_title is None:
                    logger.warning('Missing sheet ID or sheet title')
                    continue

                if sheet_title.lower() in self.sheets_to_ignore:
                    logger.info('[IgnoreSheet] SpreadsheetID: %s, Sheet: %s', spreadsheet_id, sheet_title)
                    continue

                logger.info('Processing sheet %s...', sheet_title)

                sheet_req = self.service.spreadsheets().values().get(
                    spreadsheetId=spreadsheet_id, range=sheet_title)
                sheet_resp = sheet_req.execute(num_retries=self.num_retries)

                logger.debug('Sheet %s - %s', sheet_title, sheet_resp)

                headers = None
                for row_num, row in enumerate(sheet_resp.get('values', [])):
                    if not row:
                        logger.debug('Skip empty row')
                        continue

                    logger.debug('Row - %s', row)

                    if headers is None:
                        is_header_row = all(
                            [header_column in row for header_column in required_header_columns])
                        if is_header_row:
                            logger.info('Header row find - %s', row)
                            headers = list(row)

                        continue

                    order = {
                        'spreadsheet_id': spreadsheet_id,
                        'sheet_id': sheet_id,
                        'sheet_title': sheet_title,
                        'row_num': row_num + 1
                    }
                    fields_cnt = len(row)
                    for idx, header in enumerate(headers):
                        if idx >= fields_cnt:
                            order[header] = ''
                        else:
                            order[header] = row[idx]

                    if 'fulfillment-acc' not in order or not order['fulfillment-acc'] or \
                        'customer-id' not in order or not order['customer-id']:
                        logger.info('[InvalidOrder] %s', order)
                        continue

                    fulfillment_acc = order['fulfillment-acc'].upper()
                    order['marketplace'] = fulfillment_acc[-2:]
                    order['order_id'] = order['customer-id']

                    yield order
        except errors.HttpError as error:
            logger.exception(error)

    def update_order_status(self, orders):
        if not isinstance(orders, list):
            orders = [orders]

        classified_orders = dict()
        for order in orders:
            if 'target_status' not in order:
                continue

            cur_status = order['status']
            target_status = order['target_status']
            if target_status == cur_status:
                continue

            classified_orders.setdefault(order['spreadsheet_id'], [])
            classified_orders[order['spreadsheet_id']].append(order)

        for spreadsheet_id, s_orders in classified_orders.items():
            payload = []

            for order in s_orders:
                status_range = '{}!A{}'.format(order['sheet_title'], order['row_num'])
                payload.append({'range': status_range, 'values': [[target_status]]})

            req = self.service.spreadsheets().values().batchUpdate(
                spreadsheetId=spreadsheet_id, body={'valueInputOption': 'RAW', 'data': payload})
            try:
                req.execute(num_retries=self.num_retries)
                logger.info(
                    '[OrderStatusUpdated] SpreadsheetID: %s, Orders: %s',
                    spreadsheet_id, s_orders)
            except errors.HttpError as error:
                logger.exception(error)
                logger.warning(
                    '[OrderStatusUpdateFailed] SpreadsheetID: %s, Orders: %s',
                    spreadsheet_id, s_orders)

    def update_order_confirm_date(self, orders):
        pass

    def update_order_tracking(self, orders):
        pass


if __name__ == '__main__':
    import os
    import sys
    import time

    from google.oauth2 import service_account
    from googleapiclient.discovery import build

    if len(sys.argv) < 3:
        print("You must supply a service account and google sheet id to load orders")
        sys.exit(1)

    service_account_path = os.path.abspath(os.path.expanduser(sys.argv[1]))
    if not os.path.isfile(service_account_path):
        print("Could not find file - %s" % service_account_path)
        sys.exit(1)

    creds = service_account.Credentials.from_service_account_file(service_account_path)
    service = build('sheets', 'v4', credentials=creds, cache_discovery=False)

    spreadsheet_id = sys.argv[2]

    order_loader = GoogleSheetOrder(service)
    order_statuses = dict()
    orders = []
    for order in order_loader.load_orders(spreadsheet_id):
        order_statuses[order['order_id']] = order['status']
        order['target_status'] = 'n'
        orders.append(order)

        print(order)

    order_loader.update_order_status(orders)

    time.sleep(30)

    orders = []
    for order in order_loader.load_orders(spreadsheet_id):
        if order['order_id'] in order_statuses:
            order['target_status'] = order_statuses[order['order_id']]
            orders.append(order)
    order_loader.update_order_status(orders)
