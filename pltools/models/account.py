# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from sqlalchemy import Column, String, Boolean, Integer, ForeignKey
from sqlalchemy.orm import relationship

from pltools.models import Base


class Account(Base):
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False)
    campaign_report_required = Column(Boolean, default=True, nullable=False)

    marketplace_id = Column(Integer, ForeignKey('marketplaces.id'))
    # marketplace = relationship("Marketplace", back_populates="accounts")

    # campaign_reports = relationship("CampaignReport", back_populates="account")
