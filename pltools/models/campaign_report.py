# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from sqlalchemy import Column, String, Boolean, Integer, Float, Date, ForeignKey
from sqlalchemy.orm import relationship

from pltools.models import Base


class CampaignReport(Base):
    __tablename__ = 'campaign_reports'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False, index=True)
    state = Column(String(255), nullable=False, index=True)
    status = Column(String(255), nullable=False, index=True)
    type = Column(String(255), nullable=False, index=True)
    targeting = Column(String(255), nullable=False, index=True)
    bidding_strategy = Column(String(255), nullable=False, index=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date)
    portfolio = Column(String(255))
    budget = Column(Float, nullable=False)
    impressions = Column(Integer, nullable=False)
    clicks = Column(Integer, nullable=False)
    ctr = Column(Float, nullable=False)
    spend = Column(Float, nullable=False)
    cpc = Column(Float, nullable=False)
    orders = Column(Integer, nullable=False)
    sales = Column(Float, nullable=False)
    acos = Column(Float, nullable=False)
    roas = Column(Float, nullable=False)
    report_date = Column(Date, nullable=False)

    account_id = Column(Integer, ForeignKey('accounts.id'))
    # account = relationship("Account", back_populates="campaign_reports")
