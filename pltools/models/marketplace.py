# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from pltools.models import Base


class Marketplace(Base):
    __tablename__ = 'marketplaces'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False)

    # accounts = relationship("Account", back_populates="marketplace")
