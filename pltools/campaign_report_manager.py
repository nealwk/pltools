# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com


from sqlalchemy.exc import IntegrityError

from pltools import logger
from pltools.models.marketplace import Marketplace
from pltools.models.account import Account
from pltools.models.campaign_report import CampaignReport


class CampaignReportManager():
    def __init__(self, session):
        self.session = session

    def get_reports_to_download(self, marketplace, account_name):
        account = self.get_account(marketplace, account_name)
        if not account:
            raise ValueError('Unkown account {}'.format(account_name + marketplace))

        campaign_report = (
            self.session.query(CampaignReport)
            .filter(CampaignReport.account_id == account.id)
            .order_by(CampaignReport.report_date.desc())
            .first()
        )

        today = datetime.datetime.today()
        if campaign_report:
            start_date = campaign_report.report_date + datetime.timedelta(days=1)
        else:
            start_date = today - datetime.timedelta(days=31)

        report_dates = []
        cur_date = start_date
        while cur_date < today:
            report_dates.append(cur_date)
            cur_date = cur_date + datetime.timedelta(days=1)

        return report_dates

    def is_report_imported(self, report):
        '''
        Check whether report is imported. Only account_id, name, report_date will be checked.
        '''
        account_info = report['account']
        account_name = account_info['name']
        m_name = account_info['marketplace']

        account = self.get_account(m_name, account_name)
        if not account:
            return False

        campaign_report = (
            self.session.query(CampaignReport)
            .filter(CampaignReport.account_id == account.id)
            .filter(CampaignReport.report_date == report['report_date'])
            .filter(CampaignReport.name == report['name'])
            .first()
        )

        return campaign_report is not None

    def import_report(self, report):
        '''
        Import one campaign report record
        '''
        if self.is_report_imported(report):
            return True

        account_info = report.pop('account')
        account_name = account_info['name']
        m_name = account_info['marketplace']

        account = self.get_account(m_name, account_name)
        if not account:
            return False

        result = False
        try:
            campaign_report = (
                self.session.query(CampaignReport)
                .filter(CampaignReport.account_id == account.id)
                .filter(CampaignReport.report_date == report['report_date'])
                .filter(CampaignReport.name == report['name'])
                .first()
            )
            if not campaign_report:
                campaign_report = CampaignReport(account_id=account.id)

            for k, v in report.items():
                setattr(campaign_report, k, v)

            self.session.add(campaign_report)
            self.session.commit()
            result = True
        except IntegrityError:
            self.session.rollback()
        except Exception as e:
            logger.exception(e)

        return result

    def get_account(self, marketplace, account_name):
        m = self.session.query(
            Marketplace).filter(Marketplace.name == marketplace).first()
        if not m:
            return None

        account = (
            self.session.query(Account)
            .filter(Account.name == account_name)
            .filter(Account.marketplace_id == m.id)
            .first()
        )

        return account
