# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import time
import json

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    TimeoutException, NoSuchElementException, WebDriverException,
    StaleElementReferenceException)
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys

from pltools import logger, init_web_driver, MARKETPLACE_MAPPING


class SellerCentralBase(object):
    def __init__(self, email, password):
        self.driver = init_web_driver(email)
        self.email = email
        self.password = password
        self.unpickable_marketplaces = ['jp', 'au']
        self.shared_name_domains = [MARKETPLACE_MAPPING['mx']['domain'], MARKETPLACE_MAPPING['au']['domain']]

    def is_login_required(self):
        url = self.driver.current_url
        return url.find('/ap/signin') != -1 or url.find('/ap/mfa') != -1

    def login(self):
        try:
            claimed_email_elem = self.driver.find_element_by_id('ap-claim')
            claimed_email = claimed_email_elem.get_attribute('value')

            if claimed_email.lower() != self.email.lower():
                add_account_elem = self.driver.find_element_by_id('cvf-account-switcher-add-accounts-link')
                self.driver.get(add_account_elem.get_attribute('href'))
        except NoSuchElementException:
            pass

        try:
            email_elem = self.driver.find_element_by_id('ap_email')
            email_elem.clear()

            email_elem.send_keys(self.email)

            try:
                continue_elem = self.driver.find_element_by_id('continue')
                continue_elem.click()
            except NoSuchElementException:
                pass
        except NoSuchElementException:
            pass

        time.sleep(3)

        while True:
            try:
                try:
                    remember_elem = self.driver.find_element_by_name('rememberMe')
                    if not remember_elem.is_selected():
                        remember_elem.click()
                except NoSuchElementException:
                    pass

                password_elem = self.driver.find_element_by_id('ap_password')
                password_elem.clear()
                time.sleep(1)
                password_elem.send_keys(self.password)
                time.sleep(3)
                password_elem.send_keys(Keys.RETURN)
                time.sleep(1)
                break
            except NoSuchElementException:
                break

    def pick_marketplace(self, marketplace):
        result = True

        marketplace = marketplace.lower()
        if marketplace in self.unpickable_marketplaces:
            return result

        marketplace_domain = MARKETPLACE_MAPPING.get(marketplace)['domain']
        picker_xpath = '//select[@id="sc-mkt-picker-switcher-select"]'
        target_xpath = picker_xpath + '//option[contains(text(), "{}")]'.format(marketplace_domain)
        try:
            picker_elem = WebDriverWait(self.driver, 7).until(
                EC.presence_of_element_located((By.XPATH, picker_xpath)))
            picker_elem = Select(picker_elem)
            cur_marketplace = picker_elem.first_selected_option.text.strip()
            if cur_marketplace.find(marketplace_domain) == -1:
                selected = False
            elif marketplace == 'us' and cur_marketplace in self.shared_name_domains:
                selected = False
            else:
                selected = True

            if not selected:
                marketplace_texts = [option.text.strip() for option in picker_elem.options \
                    if option.text.find(marketplace_domain) != -1]
                if marketplace == 'us':
                    for domain in self.shared_name_domains:
                        if domain in marketplace_texts:
                            marketplace_texts.remove(domain)
                if not marketplace_texts:
                    return False

                logger.debug(marketplace_texts)
                picker_elem.select_by_visible_text(marketplace_texts[0])

            WebDriverWait(self.driver, 7).until(
                EC.element_located_to_be_selected((By.XPATH, target_xpath)))
        except (NoSuchElementException, TimeoutException) as e:
            logger.exception(e)
            result = False

        return result

    def procceed_to_url(self, marketplace, url):
        self.driver.get(url)
        while self.is_login_required():
            logger.info('Login required! Trying to login...')

            self.driver.set_window_position(0, 0)

            self.login()

            wait_time = 180
            while wait_time > 0:
                wait_time -= 1
                logger.debug('Waiting for login...')
                if self.is_login_required():
                    time.sleep(1)
                else:
                    self.driver.get(url)
                    break

            if wait_time <= 0:
                logger.error('Could not login to seller central, exit!')

                return False

            time.sleep(3)

            self.driver.get(url)

        if marketplace:
            self.pick_marketplace(marketplace)

        # self.driver.set_window_position(0, 1200)

        return True

    def fill_field(self, elem, value):
        elem.clear()
        elem.send_keys(value)
