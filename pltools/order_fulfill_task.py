# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os

from google.oauth2 import service_account
from googleapiclient.discovery import build

from pltools.google_sheet_order import GoogleSheetOrder
from pltools.order_fulfiller import OrderFulfiller

from pltools import logger


class OrderFulfillTask():
    def __init__(self, cfg):
        self.cfg = cfg

        d = dict()
        service_account_path = cfg.get('google', d).get('service_account', '')
        service_account_path = os.path.abspath(os.path.expanduser(service_account_path))
        if not os.path.isfile(service_account_path):
            msg = "Could not find service account - %s" % service_account_path
            raise RuntimeError(msg)

        creds = service_account.Credentials.from_service_account_file(service_account_path)
        service = build('sheets', 'v4', credentials=creds, cache_discovery=False)
        self.gsheet_order = GoogleSheetOrder(service)

        email = cfg.get('seller', d).get('email', None)
        password = cfg.get('seller', d).get('password', None)
        if email is None or password is None:
            msg = "Missing required configuration seller>{email,password}"
            raise RuntimeError(msg)

        self.fulfiller = OrderFulfiller(email, password)

    def run(self):
        d = dict()
        seller_names = [account['name'].upper() for account in \
            self.cfg['seller'].get('accounts', [])]

        spreadsheet_ids = self.cfg.get('google', d).get('spreadsheets', [])
        if not isinstance(spreadsheet_ids, list):
            spreadsheet_ids = [spreadsheet_ids]

        try:
            for spreadsheet_id in spreadsheet_ids:
                orders = []
                for order in self.gsheet_order.load_orders(spreadsheet_id):
                    if order['status'] == 'finish':
                        continue

                    fulfillment_acc = order['fulfillment-acc'].upper()
                    if fulfillment_acc not in seller_names:
                        continue

                    fulfillment_sku = order['fulfillment-sku']

                    try:
                        result = self.fulfiller.trigger_create_fulfillment(
                            order['marketplace'], fulfillment_sku)
                        if not result:
                            logger.warning('[CreateFulfillmentFailed] order: %s', order)
                            continue

                        result = self.fulfiller.place_order(order)
                        if not result:
                            logger.warning('[PlaceOrderFailed] order: %s', order)
                            continue

                        order['target_status'] = 'finish'
                        orders.append(order)

                        logger.info('[OrderFulfilled] order: %s', order)
                    except Exception as e:
                        logger.exception(e)

                self.gsheet_order.update_order_status(orders)
        finally:
            self.fulfiller.driver.close()
