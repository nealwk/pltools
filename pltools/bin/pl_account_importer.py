# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os
import sys
import io

import click

from cmutils.config_loaders import YamlConfigLoader
from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool

from pltools import logger
from pltools.models import Session
from pltools.models.marketplace import Marketplace
from pltools.models.account import Account


@click.command()
@click.option('-c', '--config_path', help='Configuration file path.')
@click.argument('accounts_path')
def import_account(config_path, accounts_path):
    if not config_path:
        logger.error('"config_path" is required to run.')
        sys.exit(1)

    if not accounts_path:
        logger.error('"accounts_path" is required to run.')
        sys.exit(1)

    accounts_path = os.path.abspath(os.path.expanduser(accounts_path))
    if not os.path.isfile(accounts_path):
        logger.error('Could not find accounts file - %s', accounts_path)
        sys.exit(1)

    try:
        cl = YamlConfigLoader(config_path)
    except ValueError as e:
        logger.exception(e)
        sys.exit(1)

    config = cl.load()

    if 'db' not in config:
        logger.error('Missing required configuration "db"')
        sys.exit(1)

    db_name = config['db'].get('name', 'amz_campaign')
    db_host = config['db'].get('host', 'localhost')
    db_port = config['db'].get('port', 3306)
    db_user = config['db'].get('user', 'root')
    db_password = config['db'].get('password', '')
    db_url = 'mysql://{}:{}@{}:{}/{}'.format(db_user, db_password, db_host, db_port, db_name)

    campaign_engine = create_engine(db_url, echo=True, poolclass=QueuePool)
    campaign_session = Session(bind=campaign_engine)

    marketplaces = dict()
    for m in campaign_session.query(Marketplace).all():
        marketplaces[m.name] = m.id

    with io.open(accounts_path) as accounts_fh:
        for line in accounts_fh:
            account = line.strip().upper()
            if not account:
                continue

            account_name = account[:-2]
            marketplace = account[-2:]

            if marketplace not in marketplaces:
                logger.warning('[UnknownMarketplace] %s', account)
                continue

            marketplace_id = marketplaces[marketplace]
            a = (
                campaign_session.query(Account)
                .filter_by(name=account_name)
                .filter_by(marketplace_id=marketplace_id)
                .first()
            )
            if not a:
                a = Account(name=account_name, marketplace_id=marketplace_id)
                campaign_session.add(a)
                campaign_session.commit()

                logger.info('[AccountAdded] %s', account)
            else:
                logger.info('[AccountAlreadyExist] %s', account)


if __name__ == '__main__':
    import_account()
