# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import sys
import time
import pkgutil
import importlib

import click

from cmutils.config_loaders import YamlConfigLoader
from sqlalchemy import create_engine, func
from sqlalchemy.pool import QueuePool
from sqlalchemy_utils import database_exists, create_database

import pltools
from pltools import logger
from pltools.models import Base, Session
from pltools.models.marketplace import Marketplace


@click.command()
@click.option('-c', '--config_path', help='Configuration file path.')
def init_db(config_path):
    if not config_path:
        logger.error('"config_path" is required to run.')
        sys.exit(1)

    try:
        cl = YamlConfigLoader(config_path)
    except ValueError as e:
        logger.exception(e)
        sys.exit(1)

    config = cl.load()

    if 'db' not in config:
        logger.error('Missing required configuration "db"')
        sys.exit(1)

    db_name = config['db'].get('name', 'amz_campaign')
    db_host = config['db'].get('host', 'localhost')
    db_port = config['db'].get('port', 3306)
    db_user = config['db'].get('user', 'root')
    db_password = config['db'].get('password', '')
    db_url = 'mysql://{}:{}@{}:{}/{}'.format(db_user, db_password, db_host, db_port, db_name)

    campaign_engine = create_engine(db_url, echo=True, poolclass=QueuePool)

    # Create database if not exists
    if not database_exists(campaign_engine.url):
        logger.info('Database %s does not exist, try to create', db_name)
        create_database(campaign_engine.url)

        if not database_exists(campaign_engine.url):
            logger.error('Could not create database "%s"', db_name)
            sys.exit(1)

        logger.info('Database %s has been created', db_name)
    else:
        logger.info('Database %s already exits!')

    # Create missing tables
    logger.info('Creating tables...')
    for loader, name, ispkg in pkgutil.iter_modules(pltools.models.__path__):
        if ispkg:
            continue

        importlib.import_module('pltools.models.' + name)

    Base.metadata.create_all(bind=campaign_engine)
    logger.info('Tables has beed created!')

    campaign_session = Session(bind=campaign_engine)

    marketplaces = [
        'US', 'CA', 'MX', 'UK', 'DE', 'FR', 'IT', 'ES', 'JP', 'AU', 'IN', 'AE', 'TR',
        'SG', 'NL', 'SA'
    ]
    for name in marketplaces:
        marketplace = campaign_session.query(Marketplace).filter_by(name=name).first()
        if not marketplace:
            marketplace = Marketplace(name=name)
            try:
                campaign_session.add(marketplace)
                campaign_session.commit()
            except Exception as e:
                logger.exception(e)


if __name__ == '__main__':
    init_db()
