# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os
import sys
import datetime
import logging
import logging.handlers

import click

from cmutils.process_checker import ProcessChecker
from cmutils.config_loaders import YamlConfigLoader

from pltools.budget_adjust_task import BudgetAdjustTask
from pltools import logger


@click.command()
@click.option('-c', '--config_path', help='Configuration file path.')
def adjust_budget(config_path):
    if not config_path:
        logger.error('"config_path" is required to run.')
        sys.exit(1)

    try:
        cl = YamlConfigLoader(config_path)
    except ValueError as e:
        logger.exception(e)
        sys.exit(1)

    config = cl.load()

    work_dir = os.path.join('C:\\', 'Tmp', 'pltools', 'campaign_adjuster')
    config['work_dir'] = work_dir
    log_dir = os.path.join(work_dir, 'logs')
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    log_path = os.path.join(log_dir, 'campaign_adjuster.log')
    level = logging.INFO
    max_bytes = 100 * 1024 ** 2
    fh = logging.handlers.RotatingFileHandler(
        log_path, maxBytes=max_bytes, backupCount=5)
    fh.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(name)s [%(levelname)s]:%(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    pc = ProcessChecker(work_dir)
    pids = pc.get_pids()
    pids_count = len(pids)
    if pids_count <= 0:
        pid = -1
        logger.info('No campaign adjuster instance is running, start a new one')
    elif pids_count > 1:
        logger.warning('Multiple campaign adjuster instances is running, shutdown \
            all instances and start a new one')
        for pid in pids:
            pc.kill_proc(pid)
            pc.remove_pid(pid)

        pid = -1
    else:
        pid = pids[0]

    if pid == -1:
        # Existing campaign adjuster process already killed, or no instance is running, do nothing
        pass
    elif pc.is_running(pid):
        logger.info('One campaign adjuster instance is running, Exit!')
        sys.exit(0)
    else:
        logger.warning(
            'Current campaign adjuster instance is zombie, '
            'shutdown it and start a new one')
        pc.kill_proc(pid, True)
        pc.remove_pid(pid)

    # Save new campaign adjuster process information
    pc.save_pid(os.getpid())

    started_at = datetime.datetime.now()

    task = BudgetAdjustTask(config)
    try:
        task.run()
    except Exception as e:
        logger.exception(e)
    finally:
        ended_at = datetime.datetime.now()

        time_format = '%Y-%m-%dT%H:%M:%S'
        time_used = round((ended_at - started_at).total_seconds() / 3600, 2)
        logger.info(
            '[CampaignAdjusterFinished] started_at: %s, ended_at: %s, time_used: %.2fhours',
            started_at.strftime(time_format), ended_at.strftime(time_format), time_used)

        pc.remove_pid(os.getpid())


if __name__ == '__main__':
    adjust_budget()
