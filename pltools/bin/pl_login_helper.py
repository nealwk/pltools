# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import sys
import time

import click

from cmutils.config_loaders import YamlConfigLoader

from pltools import logger, MARKETPLACE_MAPPING
from pltools.seller_central_base import SellerCentralBase


@click.command()
@click.option('-c', '--config_path', help='Configuration file path.')
def login(config_path):
    if not config_path:
        logger.error('"config_path" is required to run.')
        sys.exit(1)

    try:
        cl = YamlConfigLoader(config_path)
    except ValueError as e:
        logger.exception(e)
        sys.exit(1)

    config = cl.load()

    base = SellerCentralBase(config['seller']['email'], config['seller']['password'])
    base.driver.set_window_position(0, 0)

    account = config['seller']['accounts'][0]
    marketplace = account['name'][-2:].lower()
    try:
        url = 'https://{}/cm/ref=xx_cmpmgr_dnav_xx'.format(
            MARKETPLACE_MAPPING[marketplace]['sellercentral'])
        base.procceed_to_url(marketplace, url)
        while True:
            logger.debug('Waiting for Ctrl + C to quit!')
            time.sleep(3)
    except (KeyboardInterrupt, SystemExit):
        logger.debug('KeyboardInterrupt, quitting...')
    finally:
        base.driver.quit()


if __name__ == '__main__':
    login()
