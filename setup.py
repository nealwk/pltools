# -*- coding: utf-8 -*-

# Copyright © 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md')) as f:
    long_description = f.read()

with open(path.join(here, 'pltools', 'VERSION'), 'rb') as f:
    version = f.read().decode('ascii').strip()

with open(path.join(here, 'requirements.txt')) as f:
    requirements = []
    for line in f:
        line = line.strip()
        if line and not line.startswith('#'):
            requirements.append(line)

setup(
    name='pltools',
    version=version,
    description='PL related tools.',
    long_description=long_description,
    url='https://bitbucket.org/sfds-dev/pl-tools',
    author='Neal Wong',
    author_email='neal.wkacc@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities'
    ],
    keywords='amazon seller central',
    packages=find_packages(exclude=('tests')),
    include_package_data=True,
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'pl_order_fulfiller=pltools.bin.pl_order_fulfiller:fulfill_orders',
            'pl_campaign_adjuster=pltools.bin.pl_campaign_adjuster:adjust_budget',
            'pl_login_helper=pltools.bin.pl_login_helper:login',
            'pl_init_db=pltools.bin.pl_init_db:init_db',
        ]
    }
)